## Linguagem

_Java é uma linguagem de programação e plataforma computacional lançada pela primeira vez pela Sun Microsystems em 1995. Existem muitas aplicações e sites que não funcionarão, a menos que você tenha o Java instalado, e mais desses são criados todos os dias. O Java é rápido, seguro e confiável. De laptops a datacenters, consoles de games a supercomputadores científicos, telefones celulares à Internet, o Java está em todos os lugares!_ - [Java](https://www.java.com/pt_BR/download/faq/whatis_java.xml) :coffee:

[Download Java](https://www.java.com/pt_BR/download/)

## Padrão de Arquitetura utilizado

 MVC (Model-View-Controller):
_em português modelo-visão-controlador, é um padrão de arquitetura de software (não confundir com um design pattern) que separa a representação da informação da interação do usuário com ele._ - [Wikipedia](https://pt.wikipedia.org/wiki/MVC)

[Tutorial de como criar uma aplicação em Java com o padrão MVC](http://blog.brasilacademico.com/2008/11/java-criando-uma-aplicao-em-3-camadas.html)

## Apache Maven

_Apache Maven, ou Maven, é uma ferramenta de automação de compilação utilizada primariamente em projetos Java. Ela é similar à ferramenta Ant, mas é baseada em conceitos e trabalhos diferentes em um modo diferente. Também é utilizada para construir e gerenciar projetos escritos em C#, Ruby, Scala e outras linguagens. O projeto Maven é hospedado pela Apache Software Foundation, que fazia parte do antigo Projeto Jakarta._ - [Wikipedia](https://pt.wikipedia.org/wiki/Apache_Maven)

[Tutorial de como realizar build com Maven](http://blog.caelum.com.br/processo-de-build-com-o-maven/)