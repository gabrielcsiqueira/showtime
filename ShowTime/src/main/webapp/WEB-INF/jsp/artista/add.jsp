<%-- 
    Document   : add
    Created on : 5/nov/2016, 16:10:52
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <c:import  url="/WEB-INF/jsp/header.jsp" />
    <div class="content">

        <form action="${linkTo[ArtistaController].create}" method="POST">
            <label for="nome">Nome</label>
            <br/>
            <input type="text"  name="artista.nome" id="nome"/>         
            <br/>
            <label for="stipo">Tipo</label>
            <br/>
            <input type="text"  name="tipo.uf" id="stipo"/>
            <br/>

			<label for="genero">Escolha o Genero</label>
            <br/>

            <select id="genero" name="artista.genero.id">
                <c:forEach items="${generoList}" var="gen">
                    <option value="${gen.id}">${gen.nome}</option>                                
                </c:forEach>                            
            </select>
                

            <input type="submit" value="Salvar"/>
        </form>                
    </div>
    <c:import  url="/WEB-INF/jsp/footer.jsp" />
</html>
