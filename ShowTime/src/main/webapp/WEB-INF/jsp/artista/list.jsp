<%-- 
    Document   : list
    Created on : 5/nov/2016, 16:14:51
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <table border="1">        
                <tr>
                    <th>#Id</th>
                    <th>Nome</th>
                    <th>Gênero</th>
                    <th>Tipo</th>
                    <th>Ações</th>
                </tr>
                <c:forEach items="${artistaList}" var="art" varStatus="sts">
                    <tr align="center" bgcolor="${sts.count % 2 == 0 ? 'ffff00' : 'ffffff' }">
                        <td>${art.id} </td>    
                        <td>${art.nome} </td>
                        <td>${art.tipo} </td>
                        <td>${art.genero.nome}</td>
                        <td><a href="${linkTo[ArtistaController].show(cid)}">Editar</a>| 
                            <a href="${linkTo[ArtistaController].remove(cid)}" onclick="return confirm('Deseja realmente excluir a artista?')">Remover</a> 
                        </td>


                    </tr>

                </c:forEach>
            </table>
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

