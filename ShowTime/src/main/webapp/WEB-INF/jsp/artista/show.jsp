<%-- 
    Document   : show
    Created on : 6/nov/2016, 15:59:44
    Author     : igor
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Show Time ${artista.nome}</title>


        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <form action="${linkTo[ArtistaController].update}" method="POST">
                <input type="hidden" name="artista.id" value="${artista.id}"/>
                <label for="aname">Nome</label>
                <br/>
                <input type="text" value="${artista.nome}"  id="aname" name="artista.nome" size="50" required="true" placeholder="Informe o nome do Artista" />            
                <br/>
                <label for="atipo">Tipo</label>
                <br/>
                <input type="text" value="${atleta.sobrenome}" step="0.01"  name="artista.tipo" id="atipo" />
                <br/>              
                <label for="genero">Escolha o genero</label>
                <br/>
            
                <select id="genero" name="artista.genero.id">  
                    <c:forEach items="${generoList}" var="art">
                       <option value="${art.id}">${art.nome}</option>                                
                    </c:forEach>                            
                </select>
                
                <br/>
            
                <input type="submit" value="Salvar"/>
            </form> 
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

