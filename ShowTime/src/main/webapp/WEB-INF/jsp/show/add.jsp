<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <c:import  url="/WEB-INF/jsp/header.jsp" />
    <div class="content">

        <form action="${linkTo[ShowController].create}" method="POST">
            <label for="snome">Nome</label>
            <br/>
            <input type="text"  name="show.nome" id="snome"/>         
            <br/>
            <label for="sdata">Data</label>
            <br/>
            <input type="text"  name="show.data" id="sdata"/>         
            <br/>
            <label for="shora">Hora</label>
            <br/>
            <input type="text"  name="show.hora" id="shora"/>
            <br/>
            <label for="sduracao">Duração Total</label>
            <br/>
            <input type="text"  name="show.duracao" id="sduracao"/>
            <br/>
            <label for="svalor">Valor Ingresso</label>
            <br/>
            <input type="text"  name="show.valor" id="svalor"/>
            <br/>


            <label for="local">Escolha o local</label>
            <br/>

            <select id="local" name="show.local.id">
                <c:forEach items="${localList}" var="loc">
                    <option value="${loc.id}">${loc.nome}</option>                                
                </c:forEach>                            
            </select>


            <input type="submit" value="Salvar"/>
        </form>                
    </div>
    <c:import  url="/WEB-INF/jsp/footer.jsp" />
</html>
