<%-- 
    Document   : show
    Created on : 6/nov/2016, 15:59:44
    Author     : igor
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Show Time ${show.nome}</title>


        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <form action="${linkTo[ShowController].update}" method="POST">
                <input type="hidden" name="show.id" value="${show.id}"/>
                <label for="sname">Nome</label>
                <br/>
                <input type="text" value="${show.nome}"  id="sname" name="show.nome" size="50" required="true" placeholder="Informe o nome do Show" />            
                <br/>
                <label for="sdata">Data</label>
                <br/>
                <input type="text" value="${show.data}" step="0.01"  name="show.data" id="sdata" />
                <br/>
                <label for="shora">Hora</label>
                <br/>
                <input type="text" value="${show.hora}" step="0.01"  name="show.hora" id="shora" />
                <br/>
                <label for="sdur">Duração Total</label>
                <br/>
                <input type="text" value="${show.duracaoTotal}" step="0.01"  name="show.duracaoTotal" id="sdur" />
                <br/>
                <label for="svi">Valor do Ingresso</label>
                <br/>
                <input type="text" value="${show.valorIngresso}" step="0.01"  name="show.valorIngresso" id="svi" />
                <br/>

                <label for="local">Local do show</label>
                <br/>
                <select id="local" name="show.local.id">  
                    <c:forEach items="${showList}" var="sh">
                       <option value="${sh.id}">${sh.nome}</option>                                
                    </c:forEach>                            
                </select>
                
                <br/>
            
                <input type="submit" value="Salvar"/>
            </form> 
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

