<%-- 
    Document   : list
    Created on : 5/nov/2016, 16:14:51
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <table border="1">        
                <tr>
                    <th>#Id</th>
                    <th>Nome</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Duração Total</th>
                    <th>Valor Ingresso</th>
                    <th>Local</th>
                    <th>Ações</th>
                </tr>
                <c:forEach items="${showList}" var="sh" varStatus="sts">
                    <tr align="center" bgcolor="${sts.count % 2 == 0 ? 'ffff00' : 'ffffff' }">

                        <td>${sh.id} </td>    
                        <td>${sh.nome} </td> 
                        <td>${sh.data} </td>
                        <td>${sh.hora} </td>
                        <td>${sh.duracaoTotal} </td>
                        <td>${sh.valorIngresso} </td>
                        <td>${sh.local.id}</td>
                        <td><a href="${linkTo[LocalController].show(cid)}">Editar</a>| 
                            <a href="${linkTo[LocalController].remove(cid)}" onclick="return confirm('Deseja realmente excluir o local?')">Remover</a> 
                        </td>


                    </tr>

                </c:forEach>
            </table>
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

