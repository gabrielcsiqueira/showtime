<%-- 
    Document   : list
    Created on : 5/nov/2016, 16:14:51
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Listar Gênero</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <table border="1">        
                <tr>
                    <th>#Id</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
                <c:forEach items="${generoList}" var="gen" varStatus="sts">
                    <tr align="center" bgcolor="${sts.count % 2 == 0 ? 'ffff00' : 'ffffff' }">
                        <td>${gen.id} </td>    
                        <td>${gen.descricao} </td>
                        <td><a href="${linkTo[GeneroController].show(gen)}">Editar</a>| 
                            <a href="${linkTo[GeneroController].remove(gen)}" onclick="return confirm('Deseja realmente excluir a gênero?')">Remover</a> 
                        </td>


                    </tr>

                </c:forEach>
            </table>
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

