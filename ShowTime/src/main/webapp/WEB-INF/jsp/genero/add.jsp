<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <c:import  url="/WEB-INF/jsp/header.jsp" />
    <div class="content">

        <form action="${linkTo[GeneroController].create}" method="POST">
            <label for="sdescricao">Descricao</label>
            <br/>
            <input type="text"  name="genero.descricao" id="sdescricao"/>         
            <br/>
            <input type="submit" value="Salvar"/>
        </form>                
    </div>
    <c:import  url="/WEB-INF/jsp/footer.jsp" />
</html>
