<%-- 
    Document   : show
    Created on : 5/nov/2016, 16:19:23
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Editando Genero ${genero.nome}</title>


        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <form action="${linkTo[GeneroController].update}" method="POST">
                <input type="hidden" name="genero.id" value="${genero.id}"/>
                <label for="gname">Nome</label>
                <br/>
                <input type="text" value="${genero.nome}"  id="gname" name="genero.nome" size="50" required="true" placeholder="Informe o nome do Genero" />            
                <br/>
                <label for="gdescricao">Descrição</label>
                <br/>
                <input type="text" value="${genero.descricao}" step="0.01"  name="genero.descricao" id="gdescricao" />
                <br/>

                <input type="submit" value="Salvar"/>
            </form> 
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>
