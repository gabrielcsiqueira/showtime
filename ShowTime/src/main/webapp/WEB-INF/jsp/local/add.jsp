<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show Time</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <c:import  url="/WEB-INF/jsp/header.jsp" />
    <div class="content">

        <form action="${linkTo[LocalController].create}" method="POST">
            <label for="sdescricao">Descricao</label>
            <br/>
            <input type="text"  name="local.descricao" id="sdescricao"/>         
            <br/>
            <label for="scapacidade">Capacidade</label>
            <br/>
            <input type="text"  name="local.capacidade" id="scapacidade"/>
            <br/>

            <label for="cidade">Escolha a cidade</label>
            <br/>

            <select id="cidade" name="local.cidade.id">
                <c:forEach items="${cidadeList}" var="cid">
                    <option value="${cid.id}">${cid.nome}</option>                                
                </c:forEach>                            
            </select>


            <label for="sendereco">Endereco</label>
            <br/>
            <input type="text"  name="local.endereco" id="sendereco"/>
            <br/>
            <input type="submit" value="Salvar"/>
        </form>                
    </div>
    <c:import  url="/WEB-INF/jsp/footer.jsp" />
</html>
