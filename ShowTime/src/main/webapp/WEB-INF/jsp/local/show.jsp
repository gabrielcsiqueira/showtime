<%-- 
    Document   : show
    Created on : 6/nov/2016, 15:59:44
    Author     : igor
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Show Time ${local.nome}</title>


        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <form action="${linkTo[LocalController].update}" method="POST">
                <input type="hidden" name="local.id" value="${local.id}"/>
                <label for="ldesc">Descrição</label>
                <br/>
                <input type="text" value="${local.descricao}"  id="ldesc" name="local.descricao" size="50" required="true" placeholder="Informe o nome do Local" />            
                <br/>
                <label for="lcal">Capacidade</label>
                <br/>
                <input type="text" value="${local.capacidade}" step="0.01"  name="local.capacidade" id="lcal" />
                <br/>              
                <label for="cidade">Escolha a cidade</label>
                <br/>
            
                <select id="cidade" name="local.cidade.id">  
                    <c:forEach items="${cidadeList}" var="cid">
                       <option value="${cid.id}">${cid.nome}</option>                                
                    </c:forEach>                            
                </select>
                
                <br/>

                <label for="lend">Endereço</label>
                <br/>
                <input type="text" value="${local.endereco}" step="0.01"  name="local.endereco" id="lend" />
                <br/>

            
                <input type="submit" value="Salvar"/>
            </form> 
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

