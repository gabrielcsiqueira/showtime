<%-- 
    Document   : show
    Created on : 5/nov/2016, 16:19:23
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Editando cidade ${cidade.nome}</title>


        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <form action="${linkTo[CidadeController].update}" method="POST">
                <input type="hidden" name="cidade.id" value="${cidade.id}"/>
                <label for="cname">Nome</label>
                <br/>
                <input type="text" value="${cidade.nome}"  id="cname" name="cidade.nome" size="50" required="true" placeholder="Informe o nome da Cidade" />            
                <br/>
                <label for="estado">Estado</label>
                <br/>
                <input type="text" value="${cidade.uf}" step="0.01"  name="cidade.estado" id="estado" />
                <br/>

                <input type="submit" value="Salvar"/>
            </form> 
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>
