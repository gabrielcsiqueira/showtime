<%-- 
    Document   : list
    Created on : 5/nov/2016, 16:14:51
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Listar Cidades</title>
        <c:import  url="/WEB-INF/jsp/head.jsp" />
    </head>
    <body>
        <c:import  url="/WEB-INF/jsp/header.jsp" />
        <div class="content">
            <table border="1">        
                <tr>
                    <th>#Id</th>
                    <th>Nome</th>
                    <th>Estado</th>
                    <th>Ações</th>
                </tr>
                <c:forEach items="${cidadeList}" var="cid" varStatus="sts">
                    <tr align="center" bgcolor="${sts.count % 2 == 0 ? 'ffff00' : 'ffffff' }">
                        <td>${cid.id} </td>    
                        <td>${cid.nome} </td>
                        <td>${cid.uf} </td>
                        <td><a href="${linkTo[CidadeController].show(cid)}">Editar</a>| 
                            <a href="${linkTo[CidadeController].remove(cid)}" onclick="return confirm('Deseja realmente excluir a cidade?')">Remover</a> 
                        </td>


                    </tr>

                </c:forEach>
            </table>
        </div>
        <c:import  url="/WEB-INF/jsp/footer.jsp" />
    </body>
</html>

