/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dao;

import javax.persistence.EntityManager;
import showtime.Dto.Cidade;

/**
 *
 * @author igor
 */
public class CidadeDao extends AbstractDao<Cidade> {

    public CidadeDao(EntityManager em) {
        super(em, Cidade.class);
    }

}
