/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dao;

import javax.persistence.EntityManager;
import showtime.Dto.Local;

/**
 *
 * @author igor
 */
public class LocalDao extends AbstractDao<Local> {

    public LocalDao(EntityManager em) {
        super(em, Local.class);
    }
}
