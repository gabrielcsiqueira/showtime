/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dao;

import showtime.Dto.Artista;
import javax.persistence.EntityManager;

/**
 *
 * @author igor
 */
public class ArtistaDao extends AbstractDao<Artista> {

    public ArtistaDao(EntityManager em) {
        super(em, Artista.class);
    }

}
