/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dao;

import javax.persistence.EntityManager;
import showtime.Dto.Genero;

/**
 *
 * @author igor
 */
public class GeneroDao extends AbstractDao<Genero> {

    public GeneroDao(EntityManager em) {
        super(em, Genero.class);
    }

}
