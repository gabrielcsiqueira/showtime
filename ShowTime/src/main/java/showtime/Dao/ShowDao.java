/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dao;

import javax.persistence.EntityManager;
import showtime.Dto.Show;

/**
 *
 * @author igor
 */
public class ShowDao extends AbstractDao<Show> {

    public ShowDao(EntityManager em) {
        super(em, Show.class);
    }
}
