/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author gabriel
 */
public class PersistenceManager {

    private static EntityManager em;

    private PersistenceManager() {
    }

    public static EntityManager getEntityManager() {

        if (PersistenceManager.em == null) {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistence");
            PersistenceManager.em = emf.createEntityManager();
        }
        return PersistenceManager.em;
    }
}
