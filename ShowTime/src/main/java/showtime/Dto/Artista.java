/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dto;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Check;

/**
 *
 * @author gabriel
 */
@Entity
@Check(constraints = "tipo in ('S', 'B')")
public class Artista implements AbstractDto<Integer>, Serializable {

    @Id
    @SequenceGenerator(name = "artista_id_seq", sequenceName = "artista_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "artista_id_seq")
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @Column
    @NotNull
    private String nome;
    
    @ManyToOne
    @JoinTable(name = "id_genero")
    private Genero genero;

    @Column(name = "tipo")
    private char tipo;
    
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY,targetEntity = ItemShow.class)
    private ItemShow itemShow;

    @Override
    public Integer getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the genero
     */
    public Genero getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    /**
     * @return the tipo
     */
    public char getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

}
