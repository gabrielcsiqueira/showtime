/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author igor
 */
@Entity
@Table(name = "item_show")
public class ItemShow implements AbstractDto<Integer>, Serializable {

    @Id
    @SequenceGenerator(name = "item_show_id_seq", sequenceName = "item_show_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_show_id_seq")
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @OneToMany
    @JoinColumn(name = "id_artista")
    private List<Artista> artista;

    @OneToOne
    @JoinColumn(name = "id_show")
    private Show show;

    @Column
    private String exigencia;

    @Column
    private String duracao;

    @Column
    private float cache;

    @Override
    public Integer getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the show
     */
    public Show getShow() {
        return show;
    }

    /**
     * @param show the show to set
     */
    public void setShow(Show show) {
        this.show = show;
    }

    /**
     * @return the exigencia
     */
    public String getExigencia() {
        return exigencia;
    }

    /**
     * @param exigencia the exigencia to set
     */
    public void setExigencia(String exigencia) {
        this.exigencia = exigencia;
    }

    /**
     * @return the duracao
     */
    public String getDuracao() {
        return duracao;
    }

    /**
     * @param duracao the duracao to set
     */
    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    /**
     * @return the cache
     */
    public float getCache() {
        return cache;
    }

    /**
     * @param cache the cache to set
     */
    public void setCache(float cache) {
        this.cache = cache;
    }

    /**
     * @return the artista
     */
    public List<Artista> getArtista() {
        return artista;
    }

    /**
     * @param artista the artista to set
     */
    public void setArtista(List<Artista> artista) {
        this.artista = artista;
    }

}
