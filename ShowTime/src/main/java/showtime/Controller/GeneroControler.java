/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showtime.Dao.GeneroDao;
import showtime.Dto.Genero;
import showtime.Persistence.PersistenceManager;

/**
 *
 * @author gabriel
 */
@Controller
@RequestScoped
public class GeneroControler {

    private final GeneroDao generoDao;

    private static final Logger log = LoggerFactory.getLogger(GeneroControler.class);

    @Inject
    private Result result;

    public GeneroControler() {
        this.generoDao = new GeneroDao(PersistenceManager.getEntityManager());
    }

    @Get("/genero/add")
    public void add() {

    }

    @Get("/genero/list")
    public List<Genero> list() {
        return this.generoDao.getAll();
    }

    @Post
    @Path("/genero/save")
    public void save(Genero genero) {
        this.generoDao.startTransaction();
        this.generoDao.save(genero);
        this.generoDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }

    @Post
    @Path("/genero/update")
    public void update(Genero genero) {
        this.generoDao.startTransaction();
        Genero g = this.generoDao.getById(genero);
        g.setDescricao(genero.getDescricao());
        this.generoDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }

    @Get("/genero/show/{genero.id}")
    public Genero show(Genero genero) {
        return this.generoDao.getById(genero);
    }

    @Get("/genero/remove/{genero.id}")
    public void remove(Genero genero) {
        this.generoDao.startTransaction();
        this.remove(genero);
        this.generoDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }

}
