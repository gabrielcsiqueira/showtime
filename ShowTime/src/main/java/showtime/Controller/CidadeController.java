/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showtime.Dao.CidadeDao;
import showtime.Dto.Cidade;
import showtime.Persistence.PersistenceManager;

/**
 *
 * @author gabriel
 */
public class CidadeController {

    private final CidadeDao cidadeDao;

    private static final Logger log = LoggerFactory.getLogger(CidadeController.class);

    @Inject
    private Result result;

    public CidadeController() {
        this.cidadeDao = new CidadeDao(PersistenceManager.getEntityManager());
    }

    @Get("/jsp/cidade/add")
    public void add() {

    }

    @Get("/cidade/list")
    public List<Cidade> list() {
        return this.cidadeDao.getAll();
    }

    @Post
    @Path("/cidade/save")
    public void save(Cidade cidade) {
        this.cidadeDao.startTransaction();
        this.cidadeDao.save(cidade);
        this.cidadeDao.commitTransaction();
        this.result.redirectTo(CidadeController.class).list();
    }

    @Post
    @Path("/cidade/update")
    public void update(Cidade cidade) {
        this.cidadeDao.startTransaction();
        Cidade c = this.cidadeDao.getById(cidade);
        c.setNome(cidade.getNome());
        c.setUf(cidade.getUf());
        this.cidadeDao.commitTransaction();
        this.result.redirectTo(CidadeController.class).list();
    }

    @Get("/cidade/show/{cidade.id}")
    public Cidade show(Cidade cidade) {
        return this.cidadeDao.getById(cidade);
    }

    @Get("/cidade/remove/{cidade.id}")
    public void remove(Cidade cidade) {
        this.cidadeDao.startTransaction();
        this.cidadeDao.remove(cidade);
        this.cidadeDao.commitTransaction();
        this.result.redirectTo(CidadeController.class).list();
    }

}
