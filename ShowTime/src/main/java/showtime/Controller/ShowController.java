/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import java.util.List;
import java.util.function.Predicate;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showtime.Dao.LocalDao;
import showtime.Dao.ShowDao;
import showtime.Dto.Artista;
import showtime.Dto.ItemShow;
import showtime.Dto.Show;
import showtime.Persistence.PersistenceManager;

/**
 *
 * @author gabriel
 */
public class ShowController {

    private final ShowDao showDao;
    private final LocalDao localDao;

    private static final Logger log = LoggerFactory.getLogger(ShowController.class);

    @Inject
    private Result result;

    public ShowController() {
        this.showDao = new ShowDao(PersistenceManager.getEntityManager());
        this.localDao = new LocalDao(PersistenceManager.getEntityManager());
    }

    @Get("/show/add")
    public void add() {

    }

    @Get("/show/list")
    public List<Show> list() {
        return this.showDao.getAll();
    }

    @Post
    @Path("/show/save")
    public void save(Show show) {
        this.showDao.startTransaction();
        this.showDao.save(show);
        this.showDao.commitTransaction();
        this.result.redirectTo(this).list();
    }

    @Post
    @Path("/show/update")
    public void update(Show show) {
        this.showDao.startTransaction();
        this.result.include("localList", this.localDao.getAll());
        Show s = this.showDao.getById(show);
        s.setData(show.getData());
        s.setDuracao(s.getDuracao());
        s.setHora(show.getHora());
        s.setLocal(this.localDao.getById(show.getLocal()));
        s.setValor(show.getValor());
        s.setDescricao(show.getDescricao());
        this.showDao.commitTransaction();
        this.result.redirectTo(ShowController.class).list();
    }

    @Get("/show/show/{show.id}")
    public Show show(Show show) {
        return this.showDao.getById(show);
    }

    @Get("/show/remove/{show.id}")
    public void remove(Show show) {
        this.showDao.startTransaction();
        this.showDao.remove(show);
        this.showDao.commitTransaction();
        this.result.redirectTo(ShowController.class).list();
    }

    @Post
    public void saveItemShow(Show show, ItemShow itemShow, Artista artista, float cache,
            String duracao, String exigencia) {

        Show s = this.showDao.getById(show);

        itemShow.setArtista((List<Artista>) artista);
        itemShow.setCache(cache);
        itemShow.setDuracao(duracao);
        itemShow.setExigencia(exigencia);
        itemShow.setShow(s);

        this.showDao.startTransaction();
        s.getItem().add(itemShow);
        this.showDao.commitTransaction();

        this.result.redirectTo(this).show(s);
    }

    @Get("/show/purgeItemShow/{show.id}/{itemShow.id}")
    public void purgeItemShow(Show show, final ItemShow itemShow) {
        Show s = this.showDao.getById(show);
        this.showDao.startTransaction();

        s.getItem().removeIf(
                new Predicate<ItemShow>() {
            @Override
            public boolean test(ItemShow t) {
                return t.getId().equals(itemShow.getId());
            }
        });
        this.showDao.commitTransaction();
        this.result.redirectTo(ShowController.class).show(show);
    }
}
