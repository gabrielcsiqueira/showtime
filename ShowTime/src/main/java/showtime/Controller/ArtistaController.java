/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showtime.Dao.ArtistaDao;
import showtime.Dto.Artista;
import showtime.Persistence.PersistenceManager;

/**
 *
 * @author gabriel
 */
@Controller
@RequestScoped
public class ArtistaController {

    private final ArtistaDao artistaDao;

    private static final Logger log = LoggerFactory.getLogger(ArtistaController.class);

    @Inject
    private Result result;

    ArtistaController() {
        this.artistaDao = new ArtistaDao(PersistenceManager.getEntityManager());
    }

    @Get("/artista/add")
    public void add() {

    }

    @Get
    @Path("/artista/list")
    public List<Artista> list() {
        return this.artistaDao.getAll();
    }

    @Post
    @Path("/artista/update")
    public void update(Artista artista) {
        this.artistaDao.startTransaction();
        Artista at = this.artistaDao.getById(artista);
        at.setGenero(artista.getGenero());
        at.setNome(artista.getNome());
        at.setTipo(artista.getTipo());
        this.artistaDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }

    @Post
    @Path("/artista/save")
    public void save(Artista artista) {
        this.artistaDao.startTransaction();
        this.artistaDao.save(artista);
        this.artistaDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }

    @Get
    @Path("/artista/show/{artista.id}")
    public Artista show(Artista artista) {
        return this.artistaDao.getById(artista);
    }

    @Get
    @Path("/artista/remove/{artista.id}")
    public void remove(Artista artista) {
        this.artistaDao.startTransaction();
        this.artistaDao.remove(artista);
        this.artistaDao.commitTransaction();
        this.result.redirectTo(ArtistaController.class).list();
    }
}
