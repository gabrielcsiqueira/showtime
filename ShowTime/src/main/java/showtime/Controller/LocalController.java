/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package showtime.Controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import showtime.Dao.CidadeDao;
import showtime.Dao.LocalDao;
import showtime.Dto.Local;
import showtime.Persistence.PersistenceManager;

/**
 *
 * @author gabriel
 */
public class LocalController {

    private final LocalDao localDao;
    private final CidadeDao cidadeDao;

    private static final Logger log = LoggerFactory.getLogger(LocalController.class);

    @Inject
    private Result result;

    public LocalController(LocalDao localDao) {
        this.localDao = new LocalDao(PersistenceManager.getEntityManager());
        this.cidadeDao = new CidadeDao(PersistenceManager.getEntityManager());
    }

    @Get("/local/add")
    public void add() {
        this.result.include("cidadeList", this.cidadeDao.getAll());
    }

    @Get("/local/list")
    public List<Local> list() {
        return this.localDao.getAll();
    }

    @Post
    @Path("/local/save")
    public void save(Local local) {
        this.localDao.startTransaction();
        local.setCidade(this.cidadeDao.getById(local.getCidade()));
        this.localDao.save(local);
        this.localDao.commitTransaction();
        this.result.redirectTo(this).list();
    }

    @Post
    @Path("/local/update")
    public void update(Local local) {
        this.localDao.startTransaction();
        this.result.include("cidadeList", this.cidadeDao.getAll());
        Local l = this.localDao.getById(local);
        l.setCapacidade(local.getCapacidade());
        l.setDescricao(local.getDescricao());
        l.setCidade(this.cidadeDao.getById(local.getCidade()));
        l.setEndereco(local.getEndereco());
        this.localDao.commitTransaction();
        this.result.redirectTo(CidadeController.class).list();
    }

    @Get("/local/show/{local.id}")
    public Local show(Local local) {
        this.result.include("cidadeList", this.cidadeDao.getAll());
        return this.localDao.getById(local);
    }

    @Get("/local/remove/{local.id}")
    public void remove(Local local) {
        this.localDao.startTransaction();
        this.localDao.remove(local);
        this.localDao.commitTransaction();
        this.result.redirectTo(CidadeController.class).list();
    }

}
